var plugin = requirePlugin("chatbot");
App({
  onLaunch() {
    // 使用callContainer前一定要init一下，全局执行一次即可
    // wx.cloud.init({
    //   env:'env1-5gy4sgncadb083db',
    // })
    // 
    // 插件初始化

    plugin.init({
      appid: "3IgUYDOunX7rbc7LByf9q2UwxGU9K9", //小程序示例账户，仅供学习和参考
      openid: 'oKg4j43ZXxTTzLQzPCTsEeKNfRdY', //用户的openid，必填项，可通过wx.login()获取code，然后通过后台接口获取openid
      anonymous: false, // 是否允许匿名用户评价，默认为false，设为true时，未传递userName、userHeader两个字段时将弹出登录框
      success: () => {
        console.log("app初始化成功");
      }, //非必填
      fail: (error) => {
        console.log("app初始化失败");
        console.log(error);
      }, //非必填
    });
  },
  onShow:()=>{
    // a是登录传递的参数，需要从wx.getUserInfo，和wx.login获得
    // let a ={}

    // wx.getUserInfo({
    //   success:res=>{
    //     // 把需要的参数存入a
    //     a.encryptedData=res.encryptedData
    //     a.iv=res.iv

    //     // 通过loing获取code
    //     wx.login({
    //       success:res=>{

    //         // 把code存入a
    //         a.code=res.code
    //         console.log("传递给服务器的值");
    //         console.log(a);
    //         // 发送登录请求获取token，这里没有传递头像和用户名【获取不到】
    //         wx.request({
    //           url: 'https://www.xieqinyu.com/login',
    //           method:'post',
    //           data:{
    //             code:a.code,
    //             encryptedData:a.encryptedData,
    //             iv:a.iv
    //           },
    //           header:{
    //             Authorization:'xqy '
    //           },
    //           complete:res=>{

    //             console.log("登录请求返回的值：");
    //             console.log(res);
    //             // 存储token到本地
    //             wx.setStorageSync('token', res.data.module.token)

    //           }
    //         })

    //       },


    //       fail:res=>{
    //         wx.showToast({
    //           title: '获取用户code失败',
    //           icon:'loading'
    //         })
    //       }
    //     })

    //   },
    //   fail:res=>{
    //     wx.showToast({
    //       title: '获取用户失败',
    //       icon:'loading'
    //     })
    //   }
    // })

  },
  // 全局变量
  globalData:{
    length:'全局变量测试',
    code:'',
    // 请求的基础路径
    baseUrl:'https://www.xieqinyu.com',
    // 引用图片的基础路径
    baseImg:'../../static',
    // 作物详情
    detail:'',
    // 判断token是否有效
    isToken:true,
    // 登录函数
    LOGIN(){

    }
  },
      
})
