// components/theSwiper.js
import http from '../../utils/httpRequst'
const app = getApp()
Component({
    /**
     * 组件的属性列表
     */
    properties: {
      imgUrls: Array,
    },
    /**
     * 组件的初始数据
     */
    data: {
      currentIndex: 0
    },
    /**
     * 组件的方法列表
     */

    methods: {
      swiperChange(e) {
        this.setData({
          currentIndex: e.detail.current
        });
      },
      goPro(e){
        console.log("当前图片的url");
        console.log(e.currentTarget.dataset.val);
        http(`/selectUrl?url=${e.currentTarget.dataset.val}`,'post').then(res=>{
          console.log("图片url的返回值");
          console.log(res);
          app.globalData.detail=res.data
          wx.navigateTo({
            url: '../../pages/index_detail/index_detail',
          })
        })
        // console.log(e.currentTarget.dataset.val);
      }
    }
  });
  /*
  
  <view class="dots-box own-class">
    <view class="dots {{currentIndex == index ? 'bg-333' : ''}}" wx: for="{{ imgUrls }}" wx:key="{{ index }}"></view>
  </view >
  */
  