const app = getApp()
export default(url='/',method='get',data={},formData={})=>{
  
    return new Promise((reslove,reject)=>{
        wx.request({
          url:app.globalData.baseUrl +url,
          method:method,
          data:data,
          formData,formData,
          header:{
            "Authorization":wx.getStorageSync('token')
          },
          success:(res)=>{
            if(res.data.status==403){
              wx.removeStorage({
                key: 'token',
              })
              wx.removeStorage({
                key: 'avatarUrl',
              })
              wx.removeStorage({
                key: 'nickName',
              })

            }
            reslove(res)
          },
          fail:(res)=>{
                reject(res)
          }
        })
    })

}