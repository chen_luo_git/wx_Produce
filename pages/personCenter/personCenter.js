// pages/personCenter/personCenter.js
const app = getApp()

import login from '../../utils/login';
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
import http from '../../utils/httpRequst'
import callR from '../../utils/cloudRequst'

Page({

    data:{
        // 全局变量
        globalData:'',
        // 用户数据
        userInfo:{
             nickName:'登录获取详细信息',
             avatarUrl:'/static/default_icon/个人信息_o.png'
        },
        // 用户获得点赞和评论
        baseInfo:{
          dianzan:'',
          pinglun:''
        },
        // 鼠标位移
        mouseMove:{
            moveStart:0,
            moveIng:0,
            moveEnd:0,
            moveDistance:0,
        },
        // 回弹动画效果
        Transitoin:'',
        // 当前所在下拉框位置
        flag:0,
        // 弹出层是否显示
        show:false,
        // 反馈的内容
        msg:{
          subject:'',
          content:''
        },
        // 上下滑动图标路径
        imgPicture:'../../static/centerImg/up.png',
        // https://vegetable666.oss-cn-shenzhen.aliyuncs.com/32-xiangshangjiantou 上箭头
        // https://vegetable666.oss-cn-shenzhen.aliyuncs.com/31-xiangxiajiantou 下箭头
        // 地图需要的参数
        latitude: 23.099994,
        longitude: 113.324520,
        markers: [{
          id: 1,
          latitude: 23.099994,
          longitude: 113.324520,
          name: 'T.I.T 创意园'
        }],
        covers: [{
          latitude: 23.099994,
          longitude: 113.344520,
          iconPath: '/image/location.png'
        }, {
          latitude: 23.099994,
          longitude: 113.304520,
          iconPath: '/image/location.png'
        }]
     },

     onShow:function(){
        // 全局变量赋值
        this.setData({
          "globalData":app.globalData
        })
        // 获取头像
        if(wx.getStorageSync('avatarUrl')!==''){
          this.setData({
            'userInfo.avatarUrl':wx.getStorageSync('avatarUrl'),
            'userInfo.nickName':wx.getStorageSync('nickName')
          })
        }
        // 获取点赞评论
          http('/discuss/MyComments','post').then(res=>{
            console.log("评论");
            console.log(res);
            this.setData({
              "baseInfo.dianzan":res.data.data
            })
          })
          http('/discuss/MyPraise','post').then(res=>{
            console.log('点赞');
            console.log(res);
            this.setData({
              'baseInfo.pinglun':res.data.data
            })
          })
     },
    // 检查是否登录
  ifLogin(){
    login()
  },
  //   三个鼠标事件
  handStart(e){
    this.setData({
        "mouseMove.moveStart":e.touches[0].clientY,
        "Transitoin":''
    })
  },
  handMove(e){
    // 在中间的情况
    if(this.data.flag==0){
        this.setData({
            "mouseMove.moveDistance":e.touches[0].clientY-this.data.mouseMove.moveStart,
        })
    }
    // 在上面的情况
    else{
      this.setData({
        "mouseMove.moveDistance":e.touches[0].clientY-this.data.mouseMove.moveStart-700,
    })
    }
  },
  handEnd(e){
    // 位置在中间的情况
    // console.log(this.data.mouseMove.moveDistance);
    if(this.data.flag==0){
      // 如果拉到某个节点，就固定到上方
      if(this.data.mouseMove.moveDistance<=-380){
        this.setData({
          "mouseMove.moveDistance":-700,
          "Transitoin":'0.5s',
          "imgPicture":'../../static/centerImg/dp.png',
          "flag":1
        })
      }else{
          this.setData({
            "mouseMove.moveDistance":0,
            "Transitoin":'0.5s'
          })
      }
    }
    // 在上方的情况
    else{
      if(this.data.mouseMove.moveDistance>=-320){
        this.setData({
          "mouseMove.moveDistance":0,
          "Transitoin":'0.5s',
          "imgPicture":'../../static/centerImg/up.png',
          "flag":0
        })
      }else{
          this.setData({
            "mouseMove.moveDistance":-700,
            "Transitoin":'0.5s'
          })
      }
    }
    

  },
  // 登录
  toLogin(){

    // 获取点赞评论
    http('/discuss/MyComments','post').then(res=>{
      console.log("评论");
      console.log(res);
      this.setData({
        "baseInfo.dianzan":res.data.data
      })
    })
    http('/discuss/MyPraise','post').then(res=>{
      console.log('点赞');
      console.log(res);
      this.setData({
        'baseInfo.pinglun':res.data.data
      })
    })
    

  },
  // 展示问答文章
  showMy(){
    http('/discuss/MyArticle','post').then(res=>{
      console.log("查看自己的文章");
      console.log(res);
    })
    wx.navigateTo({
      url: '/pages/myArticle/myArticle',
    })
  },
  // 智能问答
  idea(){
    wx.navigateTo({
      url: '/pages/smart/smart',
    })
    // wx.request({
    //   url: 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx5f1abcef0a0eea7b&secret=85b2a4ab1a6083609ad99318bb566628',
    //   method:'get',
    //   complete:res=>{
    //     // console.log(res);
        
    //     wx.serviceMarket.invokeService({
    //       service:'wxcae50ba710ca29d3',
    //       api:'openai',
    //       data: {
    //         "appid": '3IgUYDOunX7rbc7LByf9q2UwxGU9K9', //请前往 http://openai.weixin.qq.com 注册机器人获得
    //         "query": '白菜', //用户发出的query
    //         "name": wx.getStorageSync('nicName'), //用户昵称
    //         "avatar": wx.getStorageSync('avatarUrl') //用户头像
    //       },
    //     }).then(res=>{
    //       console.log(res);
    //     })
    //   }
    // })
  },
  // 打开弹出层【反馈】
  onUp(){
    this.setData({
      'show':true
    })
  },
  // 关闭弹出层
  onClose(){
    this.setData({
      'show':false
    })
  },
  // 反馈类型绑定
  onChangeS(e){
    this.setData({
      "msg.subject":e.detail
    })
  },
  // 反馈内容绑定
  onChangeC(e){
    this.setData({
      "msg.content":e.detail
    })
  },
  //发送邮件
  toEail(){
    http(`/mail/to?content=${this.data.msg.content}&subject=${this.data.msg.subject}`,'post').then(res=>{
      if(res.statusCode==200){
        Toast.success('发送成功');
        this.setData({
          'show':false
        })
      }else{
        Toast.fail('发送失败');
      }
    })
  }
})