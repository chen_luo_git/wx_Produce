// pages/community/community.js
import http from '../../utils/httpRequst'
import login from '../../utils/login';

Page({

    /**
     * 页面的初始数据
     */     
    data: {
      searchData:'',
      newsSuccess:false,
      news:[],
      newsCurrent:1,
      newsLoading:true,
      newsShow: false,
      newsButtonLoading:false,
      addShow:false,
      addTitle:'',
      addMain:'',
      addTitleError:'',
      addMainError:'',
      addComment:'',
      commentError:'',
      columns: ['全部', '我的', '苹果','茄子', '南瓜', '白菜'],
      // 讨论相关
      discussSuccess:false,
      discuss:[],
      disCurrent:1,
      disLimit:5,
      discussMain:{},
      disButtonLoading:false,
      discussLoading:true,
      // 讨论下的评论相关
      comments:[],
      comCurrent:1,
      comLimit:5,
      comButtonLoading:false,
      commentLoading:true,
      addType: '苹果',
      // 筛选
      type:'全部',
      currentType:'全部',
      buShow:false
    },

    // 检查是否登录
    ifLogin(){
      login()
    },
    
    onSearch(){
      this.setData({
        newsLoading:true,
        newsCurrent:1
      })
      if(this.data.searchData != '')
      {
        http('/news/html1','post',{page:this.data.newsCurrent,type:this.data.searchData}).then(res=>{
          console.log(res)
          if(res.statusCode == 200)
          {
            this.setData({
              news:res.data.data,
              newsLoading:false
            })
          }
        })
      }
      else
      {
        this.getNews()
      }
    },

    newTypeChange(event) {
      this.setData({
        addType: event.detail,
      });
    },
    typeOnClick(event) {
      const { name } = event.currentTarget.dataset;
      this.setData({
        addType: name,
      });
    },

    showNewsMain(e){
      this.setData({
        newsMain:e.currentTarget.dataset.item,
        // newsShowing:true
      })
      // http('/news/html2','post',this.data.newsMain.url).then(res=>{
      //   console.log(res)
      //   this.setData({
      //     newsContent:res.data.msg,
      //   })
      // })
      wx.navigateTo({
        url: '/pages/news/news?newsMain='+JSON.stringify(this.data.newsMain),
      })

    },
    newsLoadMore(){
      this.setData({
        newsButtonLoading:true
      })
      // if(this.data.searchData == '')
      // {
      //   http('/news/html','post',this.data.newsCurrent+1).then (res=>{
      //     console.log(res)
      //     if(res.statusCode == 200)
      //     {
      //       this.setData({
      //         news:[...this.data.news,...res.data.data],
      //         newsCurrent:this.data.newsCurrent+1,
      //         newsButtonLoading:false
      //       })
      //     }
      //     else
      //     {
      //       this.setData({
      //         newsButtonLoading:false
      //       })
      //       wx.showToast({
      //         icon:'error',
      //         title: '获取数据失败！',
      //       })
      //     }
      //   })
      // }
      // else
      // {
      //   http('/news/html1','post',{page:this.data.newsCurrent+1,type:this.data.searchData}).then(res=>{
      //     console.log(res)
      //     if(res.statusCode == 200)
      //     {
      //       this.setData({
      //         news:[...this.data.news,...res.data.data],
      //         newsCurrent:this.data.newsCurrent+1,
      //         newsButtonLoading:false
      //       })
      //     }
      //     else
      //     {
      //       this.setData({
      //         newsButtonLoading:false
      //       })
      //       wx.showToast({
      //         icon:'error',
      //         title: '数据获取失败！',
      //       })
      //     }
      //   })
      // }
    },

    // 取消筛选
    onCancel(){
      this.setData({
        addShow: false,
      });
    },
    // 确认筛选
    onConfirm(){
      if(this.data.type != this.data.currentType)
      {
        this.setData({
          currentType:this.data.type,
          disCurrent:1,
          discussLoading:true
        })
        if(this.data.type == '全部')
        {
          http(`/discuss/selectAll/${this.data.disCurrent}/${this.data.disLimit}`,'get').then(res=>{
            console.log(res)
            this.setData({
              discuss:res.data.raw,
              discussLoading:false
            })
          })
        }
        else if(this.data.type == '我的')
        {
          http('/discuss/MyArticle','post').then(res=>{
            console.log(res)
            this.setData({
              discuss:res.data.data,
              discussLoading:false
            })
          })
        }
        else
        {
          http(`/discuss/selectByType/${this.data.disCurrent}/${this.data.disLimit}?type=${this.data.currentType}`,'get').then(res=>{
            console.log(res)
            this.setData({
              discuss:res.data.raw,
              discussLoading:false
            })
          })
        }
      }
      this.setData({
        addShow:false
      })
    },

    // 展示新闻界面
    showNews(e) {
      this.setData({ 
        discussMain:e.currentTarget.dataset.item,
        newsShow: true
      });
      this.getComments()
    },
    getComments()
    {
      this.setData({
        commentLoading:true
      })
      http(`/discuss/selectOne/${this.data.comCurrent}/${this.data.comLimit}`,'get',{id:this.data.discussMain.id}).then(res=>{
        this.setData({
          comments:res.data.raw,
          commentLoading:false
        })
      })
    },
    onClose() {
      this.setData({ 
        newsShow: false,
        comCurrent:1
       });
    },
   

    showAdd(){
      this.setData({ addShow: true });
    },
    addClose() {
      this.setData({ addShow: false });
    },

    // 确认发布讨论
    addComplete(){
      if(this.data.addTitle == '')
      {
        this.setData({ addTitleError:'标题不能为空'})
      }
      else
      {
        this.setData({ addTitleError:''})
        if(this.data.addMain == '')
        {
          this.setData({ addMainError:'内容不能为空'})
        }
        else
        {
          this.setData({ 
            addMainError:'',
            discussLoading:true
          })
          http('/discuss/add','post',{comments:this.data.addMain,topic:this.data.addTitle,type:this.data.addType}).then(res=>{
            console.log(res)
            if(res.statusCode == 200)
            {
              wx.showToast({
                title: '发布成功',
                icon: 'none'
              });
              this.setData({
                type:'全部',
                currentType:'全部',
                addTitle:'',
                addMain:''
              })
              this.addClose()
              http(`/discuss/selectAll/${this.data.disCurrent}/${this.data.disLimit}`,'get').then(res=>{
                console.log(res)
                this.setData({
                  discuss:res.data.raw,
                  discussLoading:false
                })
              })
            }
            else
            {
              wx.showToast({
                title: '发布失败',
                icon: 'none',
              });
              this.setData({
                discussLoading:false
              })
            }
          })
        }
      }
    },
    // 确认发布评论
    addComment(){
      if(this.data.addComment == '')
      {
        this.setData({ commentError:'留言不能为空' })
      }
      else
      {
        this.setData({ commentError:'' })
        http(`/discuss/addDiscuss?comments=${this.data.addComment}&discussId=${this.data.discussMain.id}`,'get').then(res=>{
          console.log(res)
        })
        this.getComments()
      }
    },
    onChange(event) {
      const { picker, value, index } = event.detail;
      this.setData({
        type:value
      })
      console.log(`当前值：${value}, 当前索引：${index}`);
    },
    // 新闻与论坛版块切换
    tapChange(event) {
      if(event.detail.name == 'comment')
      {
        this.setData({
          buShow:true
        })
        if(this.data.discuss.length == 0)
        {
          this.setData({
            discussLoading:true
          })
          http(`/discuss/selectAll/${this.data.disCurrent}/${this.data.disLimit}`,'get').then (res=>{
            console.log(res)
            if(res.statusCode == 200)
            {
              this.setData({
                discuss:res.data.raw,
                discussLoading:false,
                discussSuccess:true
              })
            }
            else
            {
              this.setData({
                discussLoading:false,
              })
              wx.showToast({
                icon:'error',
                title: '数据获取失败！',
              })
            }
          })
        }
      }
      else
      {
        this.setData({
          buShow:false
        })
      }
    },
    discussReLoad(){
      this.setData({
        discussLoading:true
      })
      http(`/discuss/selectAll/${this.data.disCurrent}/${this.data.disLimit}`,'get').then (res=>{
        console.log(res)
        if(res.statusCode == 200)
        {
          this.setData({
            discuss:res.data.raw,
            discussLoading:false,
            discussSuccess:true
          })
        }
        else
        {
          this.setData({
            discussLoading:false,
          })
          wx.showToast({
            icon:'error',
            title: '数据获取失败！',
          })
        }
      })
    },
    // 获取新闻数据
    getNews(){
      this.setData({
        newsLoading:true,
        newsCurrent:1
      })
      // http('/news/html','post',this.data.newsCurrent).then (res=>{
      //   console.log(res)
      //   if(res.statusCode == 200)
      //   {
      //     this.setData({
      //       news:res.data.data,
      //       newsLoading:false,
      //       newsSuccess:true
      //     })
      //   }
      //   else
      //   {
      //     this.setData({
      //       newsLoading:false,
      //     })
      //     wx.showToast({
      //       icon:'error',
      //       title: '数据获取失败！',
      //     })
      //   }
      // })
      this.setData({
        news:[{
          "title" : "湖南湘乡：创新农业管理模式，夯实粮食安全基础！ ",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "今年以来，湖南省湘乡市便在积极主动地探索创新农田管理新模式，来积极引导种粮大户们采用统分结合的方式，对粮食生产管理进行探索。",
          "html":`<h1>湖南湘乡：创新农业管理模式，夯实粮食安全基础！</h1>
          <p>发布日期：2022-07-09  来源：原创  浏览次数：890</p>
          <p>今年以来，湖南省湘乡市便在积极主动地探索创新农田管理新模式，来积极引导种粮大户们采用统分结合的方式，对粮食生产管理进行探索新模式，保障粮食安全。</p>
          <p>目前，统分结合的方式已经在湘乡市种粮大户中推广，并为农户们带来更多的服务和帮助。为有效提高农业生产质量，强化农业管理，湘乡市也针对部分耕地细碎化的问题进行整合，这样不仅有效地降低了农业机械化作业的成本，还让农业生产更规模化，更方便管理，提高农业生产效率。</p>
          <p>为了避免因管理不到位导致粮食单产下降的情况发生，今年以来，湘乡市创新采用统分结合的粮食生产管理模式，由种粮大户统一流转土地、采购农资、翻耕播种、收割销售，按100到300亩不等的面积划片分区，由专人进行管理。同时，还给农户们带来务工增收的就业机会，让不少的村民们尝到了甜头。</p>
          <p>据介绍，大户集中承包耕地，有利于大规模机械化作业；而把大片田划片分区，则可以更加精细化管理，进一步提高粮食产量。并且在机械化作业的推动下，“人管+技管”的方式也为粮食丰收奠定了良好的基础，确保粮食稳产增收。</p>
          `,
          "time" : "2022-07-09"
        },{
          "title" : "新疆伽师新梅产业园铺开乡村振兴路",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "家住新疆维吾尔自治区喀什地区伽师县拉依力克村的吐尔逊沙吾提和妻子艾米地古丽胡吉一大早打理完自家的两座拱棚后，便来到村头的...",
          "html":`<h1>新疆伽师新梅产业园铺开乡村振兴路</h1>
          <p>发布日期：2022-07-09  来源：农民日报  浏览次数：899</p>
          <p>吐尔逊·沙吾提现如今家中多半的收入来自这个新梅产业园。去年伊始，伽师县与广东省对口支援新疆工作前方指挥部依托当地新梅产业优势，打造出以新梅为主的集交易、分拣、保鲜、冷藏、包装、物流、研发和培训为一体的全产业链特色产业园区。吐尔逊在产业园里负责冷藏保鲜库的日常管理，而妻子艾米地古丽是包装车间流水线上的一名工人。</p>
          <p>如今，像吐尔逊夫妻俩这样在该产业园务工的农民有近1000名。夏天鲜杏大量上市的时节，吐尔逊和他的同伴们每天忙着将鲜杏分拣、包装、冷藏后销往各地。“现在还不算太忙，趁着小麦已收割完毕，我每天还能早晚回家打理我那两拱棚瓜，等到新梅成熟时我们这里会更忙些，好在那时拱棚里的瓜已经卖得差不多了。”吐尔逊把一年的时间安排顺当，不错过任何挣钱的好时机。</p>
          <p>一个现代农业特色产业园让这么多村民不误农时地加入产业工人的大军，解决就业难的问题，对当地来说确实是一件大好事。</p>
          <p>“为了让新梅产业行稳致远，真正成为群众稳定增收和巩固拓展脱贫攻坚成果同乡村振兴有效衔接的重要支点，一二三产融合发展产业链是关键。”县委副书记王永把目光放得更远。</p>
          <p>这个占地365亩、投资3.8亿元的产业园于2021年建成，经过两年的扩建，已成为一个集技术科研、种植示范、冷藏物流、精深加工、品牌销售、生态旅游等六个功能区为一体的现代化农业产业园区，年分级包装、加工新梅1.7万吨，销售额3.12亿元。在一批行业领军企业相继落户产业园后，新鲜的新梅在园区恒温车间里完成自动化筛选、包装等环节，8小时到12小时后就可经物流运输通道销往全国各地。那些品相不是太好的鲜果则被加工成新梅产品，经过统一包装后成为了伽师新梅产业链上重要的盈利点。</p>
          <p>吐尔逊·沙吾提所在的英买里乡是伽师县新梅主产区。利用这一特色优势产业，该乡由曾经的深度贫困乡一跃成为了乡村振兴示范乡，引领带动全县新梅产业逐步做大做强，打开了产业振兴的财富之门。目前，全县种植新梅45万亩，挂果面积23万亩，面积占全国的40%，产量占到60%，是全国最大的优质新梅生产加工和出口基地。</p>
          <p>据了解，不单单是新梅产业园，伽师瓜种植基地、大棚蔬菜培育中心、设施农业产业园、特色种养合作社、休闲观光等一大批特色现代农业园如同雨后春笋般涌现并不断发展壮大。</p>
          <p>王永表示，现代农业产业园采用“基地+农户+企业”的方式，从精选苗木到指导农户科学管理、从水肥数控到加工冷链再到物流销售，引导科学种植提质增效的同时还带动当地劳动力就业，延长精深加工产业链条，为伽师的农业产业振兴插上了打造品牌、拓展销路、带动就业的翅膀。</p>
          `,
          "time" : "2022-07-09"
        },{
          "title" : "从“会种地”到“慧种地”",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "当前，正是早稻生长关键期。配药、装药、安电池，用手机连接无人机信号湖南省耒阳市金绿丰农机专业合作社绿油油的稻田旁，种粮大...",
          "html":`<h1>新疆伽师新梅产业园铺开乡村振兴路</h1>
          <p>当前，正是早稻生长关键期。</p>
          <p>配药、装药、安电池，用手机连接无人机信号……湖南省耒阳市金绿丰农机专业合作社绿油油的稻田旁，种粮大户梁满绿带着90后的女儿、女婿，正在为水稻喷洒农药。一台满装40公斤农药的无人机腾空而起，在稻田上空来回穿梭，操作手只需调好参数，喷药就能自动进行。</p>
          <p>这已经是梁满绿用的第三代植保无人机。他啧啧赞叹：“一代比一代强!几年前的无人机还需手动控制，现在的机器完全自主飞行，自动避障，不仅喷雾均匀、节省农药，而且效率更高，一天的喷药面积够人工干20多天。”</p>
          <p>从“会种地”变身“慧种地”，近年来，粮食大省湖南大力建设智慧智能农机产业链，开展智慧农业示范创建，三湘大地上，一场农业智慧化、数字化、无人化的变革正在探索中前行。</p>
          <p>“种田15年，从来没有这样轻松过。”梁满绿说。</p>
          <p>2007年春节刚过，在外打工10多年的梁满绿和妻子王国萍商量留在老家，在分析了养鱼、养猪、养鸡鸭等选项后，夫妻俩毅然决定种粮。</p>
          <p>“当初选择种田，是想着技术门槛低，我有的是力气，自己播种、除草、施肥、打药、收割，应该有赚头。”当年秋天，梁满绿试种的1.3亩水田收获稻谷1600多斤，刨除化肥、农药、种子等成本，不算劳力，亩收入800元。看到效益划算，第二年开春，夫妻俩四处联系，流转稻田100多亩。</p>
          <p>种植面积扩大，从播种到收割全靠自己肯定不行了。2008年春天，梁满绿夫妇在农机补贴政策的支持下，买下了一套大型农机。随着种田效率大大提升，夫妻俩种田面积越来越大，购置的农机也越来越多。</p>
          <p>2014年，梁满绿联合耒阳市泗门洲镇7个种田大户，组建耒阳市金绿丰农机专业合作社，如今已购置各类农业机械40多台套，总价值400多万元。有了机械化赋能，合作社流转种植水稻最高的年份达到5000多亩，社员们还对外承揽机耕、机插、飞防、机收、粮食烘干和病虫害综合防治等业务，服务周边数万亩农田。</p>
          <p>梁满绿所在的耒阳市，农业机械化水平也不断升级。目前全市主要农作物综合机械化水平达到74%，水稻耕种收综合机械化水平达到81.8%，农机总量达7.81万台(套)。</p>
          <p>机械多了，梁满绿开始琢磨学习技术。尽管他文化水平不高，但说起农机驾驶、操作头头是道。两年前，能打药能施肥播种的农业无人机流行，夫妻俩花了近20万元，一下子就买了5台。梁满绿报名参加无人机培训，练习飞行操作，终于拿到了“植保无人机系统操作许可证”。</p>
          <p>农忙之余，梁满绿夫妻俩对科技种田新闻特别关心。“会种田，不如‘慧种田’，我十几年种粮经历就见证了农业机械的普及，我觉得智慧种田离我们不会远。”梁满绿说。</p>
          <p>去年，梁满绿督促在外打工的女儿、女婿学习无人机操作技能，领取无人机操作许可证。今年，他又让女儿、女婿春耕期间回到农村，学习大棚育秧全套技术。“智慧农业的前端无人，更需要后端有‘高’人。”梁满绿说，现在愿意下地种田的劳力越来越少，人工费越来越贵，年轻人掌握操作高科技农机、“智慧种田”的一技之长，农业的根脉才不会断，粮食安全才有保障。</p>
          `,
          "time" : "2022-07-09"
        },{
          "title" : "好山好水好生活",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "打造万亩林场，修建水泥村道，发展绿色农业青山叠翠，绿水潺潺，站在坵演大桥前，鄢仁河谈起30多年来村里的发展变化，难掩激动...",
          "html":`<h1>好山好水好生活</h1>
          <p>发布日期：2022-07-09  来源：农民日报  浏览次数：895</p>
          <p>“打造万亩林场，修建水泥村道，发展绿色农业……”青山叠翠，绿水潺潺，站在坵演大桥前，鄢仁河谈起30多年来村里的发展变化，难掩激动，“我在村里做了30多年村干部，村里点点滴滴变化，我都亲身感受。”</p>
          <p>福建省永泰县坵演村距福州市区约100公里，是革命老区基点村，有7个自然村、2200多人。由于地处偏远，20世纪七八十年代，村民基本靠砍树种菇为生。这一过度消耗林木资源的产业，不仅没有让村民富起来，反而出现了不少“光头山”，导致生态失衡。随着年轻人不断外出谋生，坵演村渐渐失去活力。</p>
          <p>20世纪90年代初，坵演村围绕“山顶林戴帽、山中果缠腰、山下吨粮田”进行综合立体开发，为恢复山林，村里成立护林队，不定时在山间巡逻，禁止村民乱砍滥伐。在相关部门支持下，实施森林生态补偿政策，每亩林地按照26元标准对村民进行生态补偿。3万多亩的生态林，给村民带来直接收益的同时，也为村集体带来相对稳定收入。</p>
          <p>在大樟溪畔的小坪自然村，一棵400多年的榕树，如同一把巨伞，为村民遮风挡雨。离榕树不远处是一片油杉林，46棵300年以上树龄的油杉高耸入云、生机勃勃。青山傍绿水，村旁蜿蜒的大樟溪将村里如画美景连串成线。</p>
          <p>“以前村里只有一条沙土路，又弯又窄，晴天灰尘四起，雨天泥泞不堪。”曾几何时，交通一直是制约坵演村发展的老大难问题。为此，坵演村开始了坵演大桥的建设，2004年，长140米、宽3.7米的坵演大桥建成。2013年，通过上级拨款、村民集资，坵演村通往潭头自然村、上岭自然村等多条村道完工。</p>
          <p>橄榄园区连绵起伏，百香果园区竹架林立，茶油树园区郁郁葱葱……将生态建设成果转化为经济发展优势，坵演村成立农业专业合作社，以“公司+合作社+农户”的形式，吸纳村民成为社员，因地制宜发展特色橄榄、百香果、青梅、李果、油茶等特色农业产业。随着种植规模的扩大，村里还通过招商引资，建起了多个初级农产品加工厂，其中，橄榄加工厂每年就能为村民带来20多万元经济效益。</p>
          <p>天然温泉、古厝老宅、红色记忆……近几年，有着丰厚旅游资源的坵演村确立了旅游兴村之路。随着坵演村初心馆、红色文化培训中心、特色木屋民宿、农业观光园、游客服务中心等项目启动建设，这个有着国家森林乡村、省级乡村振兴试点村、省级森林村庄等美誉的村庄，正吸引着越来越多的游客前往休闲观光。</p>
          <p>“牢记嘱托，将‘生态绿’和‘老区红’相结合，乡村旅游产业有着很好的前景。”坵演村党支部书记陈掌平干劲十足地说，“坵演村再也不是那个与世隔绝的深山贫困村，她如同一颗珍珠，闪耀在秀丽的大樟溪畔。”</p>
          `,
          "time" : "2022-07-08"
        },{
          "title" : "围绕县域经济办培训",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "近日，黑龙江省青冈县高素质农民培育发展一业一品、实现兴村富民目标培训班，克服疫情带来的影响，完成各项培训任务，顺利结业。",
          "html":`<h1>围绕县域经济办培训</h1>
          <p>近日，黑龙江省青冈县高素质农民培育发展一业一品、实现兴村富民目标培训班，克服疫情带来的影响，完成各项培训任务，顺利结业。培训班由黑龙江省青冈县农业农村局主办，黑龙江省农民创业促进会承办，全县村党支部和村集体经济组织负责人、各类农村经济组织的创办人或负责人、种植大户、已经脱贫的人员等共计700人参加了培训。</p>
          <p>培训采取线上学习、课堂面对面授课和现场实训考察学习相结合的方式进行。教学内容紧紧围绕青冈县县域经济发展的总思路，紧紧围绕实现兴村富民的总体战略目标，开展了包括“黑龙江省主要经济作物品种及栽培技术模式”“农业种植业的法律风险与防范措施”“成功的营销，从提升内在素质和塑造外在形象开始”“鲜食玉米的品种选择和栽培技术”“玉米提质增效栽培技术”“工业大麻的田间栽培技术和前景分析”“依托资源优势走出一业一品发展的十字原则和路径”“用身边人身边事谈创业创新发展”“土地托管是实现农业现代化的必然选择”“乡村振兴的战略目标和八大着力点”等11个课题的授课。学员通过系统学习提升了对巩固脱贫攻坚成果接续乡村振兴振兴战略的认识，坚定了通过践行一业一品、实现兴村富民的决心和勇气。</p>
          <p>通过现场实训考察学习，学员被成功创业者的创业历程和取得的创业成果所鼓舞，增强了创业信心。大家纷纷表示，幸福的生活是自己创造出来的，乡村振兴的核心是产业振兴，只有产业振兴才能够实现兴村富民的目标。</p>
          `,
          "time" : "2022-07-08"
        },{
          "title" : "云南元江：农技输出及时，稳保收成更上一层楼",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "为有效促进粮食增产、果业增效，今年，云南省玉溪市元江县因地制宜也在不断探索新的农业发展模式，调整优化农业种植结构，投入更...",
          "html":`<h1>云南元江：农技输出及时，稳保收成更上一层楼</h1>
          <p>发布日期：2022-07-04  来源：原创  浏览次数：8178</p>
          <p> 为有效促进粮食增产、果业增效，今年，云南省玉溪市元江县因地制宜也在不断探索新的农业发展模式，调整优化农业种植结构，投入更多的农业科技技术，探索出更高效种植的现代化发展道路，促进农业生产提质增效。</p>
          <p>据了解，今年在农业部门的帮助下，元江县曼来镇曼来社区也高效高质的完成了早稻，并且给农户们提供了更方便的灌溉设施，为农业管理提供更优质的服务。同时，还派出农技人员深入到田间地头，来帮助农户们更科学合理的做好相关的农业生产，促进水稻生长更优质。</p>
          <p>据悉，今年元江县夏种粮食（秋粮）作物面积23.5万亩，完成任务数的106%。目前，农业部门正因地制宜调整优化农业种植结构，积极探索节水抗旱稻配套栽培技术，并结合各乡镇的水土环境，做到选种、人员、技术三到位，倡导农户发展现代农业套种新模式，促进粮食增产、果业增效。</p>
          <p>据农技人员的分析下，针对今年前期雨水偏多以及气温偏低的特点，来为农作物制定出更具针对性的管理方案，帮助农户们做好相关的技术培训，确保水稻种的好，收成好。</p>
          `,
          "time" : "2022-07-08"
        },{
          "title" : "夏季田管进行时：提单产、保丰收 我国秋粮长势总体正常",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "人民网北京7月7日电(记者李栋)小暑已至，万物蓬勃。眼下，各地多措并举抓夏管，确保秋粮在拔节期顺利生长。据农业农村部农情调度...",
          "html":`<h1>围绕县域经济办培训</h1>
          <p>发布日期：2022-07-07  来源：人民网  浏览次数：8184</p>
          <p>人民网北京7月7日电 (记者李栋)小暑已至，万物蓬勃。眼下，各地多措并举抓夏管，确保秋粮在拔节期顺利生长。据农业农村部农情调度显示，秋粮作物长势总体正常。</p>
          <p>夏管的高效离不开“科技范”。“植保无人机一次可以有效载荷30公斤生物制剂，能自主规划航线，操作起来很简单，一天作业面积超300亩。”在贵州仁怀市的一高粱基地里，村里的农机手夏燕指着盘旋在空中的无人机介绍，“无人机不仅减少了人员在田间穿梭造成的踩踏损坏，还能大大节省了人工成本费用。”</p>
          <p>“2022年仁怀市农业机械投入设备达16万台套，农业机械化率达47%。”仁怀市农业农村局农业技术综合服务站助理农艺师徐立表示，通过培训，机械化的应用大大提高了产业发展的效率，为当地农业现代化迈出了坚实的一步。</p>
          <p>夏管时节，做好田间管护确保秋季增产增收，就要打赢“稳粮增豆”攻坚战。“间作种植，玉米不减产，还多种了一季大豆，能增加不少收入。”河北省邯郸市种粮大户余明健站在玉米地里，仔细查看着油绿发亮的大豆苗和玉米苗。</p>
          <p>推行玉米大豆带状复合种植，能实现“同一块地大豆、玉米共生，一季双收”。同时，充分发挥大豆固氮养地作用，改善土壤条件，达到节本增产、农户增收的多重效果。“大豆玉米复种新模式，一台免耕播种机，可一次性播种、精量施肥，一趟下来就完成了多个步骤，大大节省了时间和人力成本。”当地农技人员介绍。</p>
          <p>据农业农村部农情调度显示，目前，东北、西北地区春玉米大部处于拔节至喇叭口期，中稻大部处于分蘖至拔节期，夏玉米、夏大豆等作物处于出苗生长期，秋粮作物长势总体正常。</p>
          <p>“我国地域辽阔，农作物栽培和选种等技艺具有多样性，要通过加强农机农艺融合，加强集成配套的全程机械化技术体系研究。探索适合不同作物、不同区域、不同规模的全程机械化生产模式，推动建立健全区域化、标准化的高质量粮食机械化生产体系。”农业农村部农业机械化总站信息处副处长朱礼好说。</p>
          `,
          "time" : "2022-07-07"
        },{
          "title" : "青海：绿肥生产促进农业生产“一举多得”",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "6月21至22日，国家绿肥产业技术体系西宁综合试验站工作人员赴贵德县金穗源种植合作社、青海富禾源农牧科技开发有限公司、民和金...",
          "html":`<h1>青海：绿肥生产促进农业生产“一举多得”</h1>
          <p>发布日期：2022-07-07  来源：原创  浏览次数：8189</p>
          <p>6月21至22日，国家绿肥产业技术体系西宁综合试验站工作人员赴贵德县金穗源种植合作社、青海富禾源农牧科技开发有限公司、民和金马种植专业合作社开展毛叶苕子相关技术指导工作。他们还相继查看了毛苕子之中、毛苕子麦秸协同换田等相关试验示范，这些产业也切实带动农牧业更高质量的发展。</p>
          <p>当前，绿肥生产发展快速，如何实现绿肥种业保障是关键性瓶颈。西宁综合试验站也高度重视毛叶苕子繁种工作，民和回族土族自治县、贵的县毛叶苕子制种面积近300亩(1亩≈0.067公顷)，民和金马种植专业合作社负责人表示，开展绿肥专家推荐的毛苕子小麦混播模式，不仅能减少化肥用量，还促进苕子种子增收。</p>
          <p>据了解，贵德光热资源丰富，是青海省果品发展的核心区域，西宁综合试验站构建的果园兼做绿肥周年生产模式已经在该地区大面积推广。同时，“小麦+绿肥+牛羊”的中央循环模式，也有效地减少肥料使用，将小麦/青稞粉及牛羊肉品质得到提升，还促进了品牌发展，切实带动更多农户增收。</p>
          <p>接下来，该站也将按照“生态循环、做大做强、提升效益”的要求，青海河湟灌区贵德、民和、尖扎等县，注重产业生态化发展，西宁综合试验站也将因地制宜打造地域特色，来支撑农业绿色发展。</p>
          `,
          "time" : "2022-07-07"
        },{
          "title" : "孤老优抚对象老有所养老有所乐",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "近年来，山东省乐陵市大力推进光荣院建设，解决农村孤老优抚对象住房难、生活难、医疗难和精神文化生活单调的问题。该市通过高定...",
          "html":`<h1>青海：绿肥生产促进农业生产“一举多得”</h1>
          <p>近年来，山东省乐陵市大力推进光荣院建设，解决农村孤老优抚对象住房难、生活难、医疗难和精神文化生活单调的问题。该市通过高定位投资打造、高标准建设运营、精细化看护照料、多元化关怀服务，办好农村孤老优抚对象养老事业，实现“老有所养、老有所医、老有所乐”。</p>
          <p>乐陵对全市17个乡镇(街道)孤老优抚对象的需求意见进行摸底调查，逐一建立孤老优抚对象档案，并投资2300万建设集医养康乐于一体的综合型区域性光荣院。光荣院在职护工、厨师等工作人员全部持证上岗，熟练掌握包含老人衣食住医乐的16项精细服务成为全院职工的必备技能。围绕健康养生，配齐功能用房，购置健身器材及医疗设备，为每位老人建立健康档案，形成“晨检问询、健康宣教、养生座谈、定期体检”四位一体的健康保健服务模式。推进品质养老、科学饮食、心灵关怀，定期邀请心理咨询师坐诊提供心理疏导，工作人员日常陪老人聊天解闷，并在生日、重阳节等重要节日送上关怀祝福。根据兴趣爱好购置书画工具、棋牌球类等休闲娱乐设施，并推出固定学习日、象棋比赛、“乐此一游”等文体活动，丰富他们的晚年生活。</p>
          `,
          "time" : "2022-07-06"
        },{
          "title" : "抢农时 促生产 全国有序推进夏管夯实全年粮食丰收基础",
          "url" : "http://www.xieqinyu.com:8081",
          "content" : "三分种，七分管，十分收成才保险。仲夏时节，正值夏季秋作物生长、管理的关键时期。最新农情调度显示，除双季晚稻尚未栽插外，全...",
          "html":`<h1>抢农时 促生产 全国有序推进夏管夯实全年粮食丰收基础</h1>
          <p>发布日期：2022-07-05  来源：人民网  浏览次数：8198</p>
          <p>三分种，七分管，十分收成才保险。仲夏时节，正值夏季秋作物生长、管理的关键时期。最新农情调度显示，除双季晚稻尚未栽插外，全国夏播基本结束。眼下，全国夏管正有序推进，各地强化指导服务，不断提升粮食生产管理水平，夯实粮食稳产基础。</p>
          <p>“我家种了600多亩玉米，出苗率较高，补种少。上半月持续高温，农业专家指导要用浅水灌溉，效果较好。”河南省安阳市内黄县楚旺镇种粮大户杨春林说，做好病虫害防治、肥水管理、化学除草等方面夏管工作，有信心迎来今年玉米大丰收。</p>
          <p>夏种夏管是全年粮食生产承上启下的关键时期。近几天的持续降水，有效缓解了高温天气对农作物生长带来的影响。“通过推进“百名科技人员包百村”活动，针对夏季秋作物生长关键期，病虫害易发生的情况，抓早抓小，采取科学施肥、综合灭草等有效举措，高质量抓好夏管促增产。”安阳市农业农村局相关负责人介绍，内黄县52.06万亩玉米已陆续进入小喇叭口期，也迎来了苗期田间管理的关键时期。</p>
          <p>最新农情调度分地区来看，东北地区水稻、玉米进入拔节期，大豆进入始花期，长势总体正常。近日，农业农村部部署安排早稻后期田管和“双抢”工作。“各地充分认识夺取早稻丰收的重要性，增强责任感紧迫感，全力以赴抓好早稻后期田管和“双抢”工作，确保早稻安全成熟、丰收到手，确保双季晚稻适期栽种、栽足栽满，为赢得全年粮食丰收打好基础。”相关负责人表示。</p>
          `,
          "time" : "2022-07-06"
        }],
        newsLoading:false,
        newsSuccess:true
      })
    },
   //点赞
    like(e){
      this.setData({
        good:!(this.data.good)
      })
      for(let i=0;i<this.data.discuss.length;i++)
      {
        if(this.data.discuss[i].id == e.currentTarget.dataset.id)
        {
          this.setData({
            [`discuss[${i}].ispraise`]:!this.data.discuss[i].ispraise,
          })
          if(this.data.discuss[i].ispraise)
          {
            this.setData({
              [`discuss[${i}].praise`]:this.data.discuss[i].praise+1
            })
            http('/discuss/praise','post',{discussId:e.currentTarget.dataset.id}).then(res=>{
              console.log(res)
              if(res.statusCode == 500)
              {
                wx.showToast({
                  title: '点赞失败',
                  icon: 'none',
                });
              }
              else
              {
                wx.showToast({
                  title: '点赞成功',
                  icon: 'none',
                });
              }
            })
          }
          else
          {
            this.setData({
              [`discuss[${i}].praise`]:this.data.discuss[i].praise-1
            })
            http(`/discuss/praise1?discussId=${e.currentTarget.dataset.id}`,'delete').then(res=>{
              console.log(res)
              if(res.statusCode == 500)
              {
                wx.showToast({
                  title: '取消点赞失败',
                  icon: 'none',
                });
              }
              else
              {
                wx.showToast({
                  title: '取消点赞成功',
                  icon: 'none',
                });
              }
            })
          }
          
        }
      }
    },
    disLoadMore(){
      if(this.data.currentType == '我的')
      {
        this.setData({
          disButtonLoading:true
        })
        http('/discuss/MyArticle','post').then(res=>{
          console.log(res)
          this.setData({
            discuss:res.data.data,
            disButtonLoading:false
          })
        })
      }
      else
      {
        this.setData({
          disButtonLoading:true
        })
        if(this.data.currentType == '全部')
        {
          http(`/discuss/selectAll/${this.data.disCurrent+1}/${this.data.disLimit}`,'get').then(res=>{
            console.log(res)
            if(res.statusCode == 200)
            {
              this.setData({
                discuss:[...this.data.discuss,...res.data.raw],
                disCurrent:this.data.disCurrent+1,
                disButtonLoading:false
              })
            }
            else
            {
              this.setData({
                disButtonLoading:false
              })
              wx.showToast({
                icon:'error',
                title: '获取数据失败！',
              })
            }
          })
        }
        else
        {
          http(`/discuss/selectByType/${this.data.disCurrent+1}/${this.data.disLimit}?type=${this.data.currentType}`,'get').then(res=>{
            console.log(res)
            if(res.statusCode == 200)
            {
              this.setData({
                discuss:[...this.data.discuss,...res.data.raw],
                disCurrent:this.data.disCurrent+1,
                disButtonLoading:false
              })
            }
            else
            {
              this.setData({
                disButtonLoading:false
              })
              wx.showToast({
                icon:'error',
                title: '获取数据失败！',
              })
            }
          })
        }
      }
    },
    comLoadMore(){
      this.setData({
        comButtonLoading:true
      })
      http(`/discuss/selectOne/${this.data.comCurrent+1}/${this.data.comLimit}`,'get',{id:this.data.discussMain.id}).then(res=>{
        if(res.statusCode == 200)
        {
          this.setData({
            comments:[...this.data.comments,...res.data.raw],
            comCurrent:this.data.comCurrent+1,
            comButtonLoading:false
          })
        }
        else
        {
          this.setData({
            comButtonLoading:false
          })
          wx.showToast({
            icon:'error',
            title: '获取数据失败！',
          })
        }
      })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      http('/news/html','post').then(res=>{
        console.log("获取数据");
        console.log(res);
      })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
      this.getNews()
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
  
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})