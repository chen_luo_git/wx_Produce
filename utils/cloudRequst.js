
export default (url='/',method='get',data={})=>{

    return new Promise((reslove,reject)=>{
        wx.cloud.callContainer({
            path:url,
            method:method,
            data:data,
            header:{
              'x-wx-SERVICE':'cheshi',
              'content-type': 'application/json'
            },
            success:(res)=>{
                reslove(res)
            },
            fail:(res)=>{
                reject(res)
            }
          })
    })
}