// pages/index_allProduct/index_allProduct.js
const app = getApp()
import http from '../../utils/httpRequst'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        productData:[],
        globalData:''
    },
    // 点击作物的详细信息
    detail:function(val){
        console.log("作物的详细信息");
        console.log(val);
        // 记录当前点击的作物信息，保存到全局变量里面
        app.globalData.detail=val.target.dataset.val
        wx.navigateTo({
          url: '/pages/index_detail/index_detail',
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        // 全局变量赋值
        this.setData({
            "globalData":app.globalData
        })
        // 请求作物列表
        http('/selectAll','post').then(res=>{
            console.log("获取的所有蔬菜信息");
            console.log(res);
            this.setData({
                'productData':res.data
            })
        })
    },

    // 搜索作物
    onSearch(e){
        http(`/selectVe?name=${e.detail}`,'post').then(res=>{
            this.setData({
                'productData':res.data
            })
        })
    }

    
})