// index.js
// 获取应用实例
const app = getApp()
import http from '../../utils/httpRequst'
import login from '../../utils/login';
import loign from '../../utils/login'
import Dialog from '/@vant/weapp/dialog/dialog';
Page({
  data:{
    // 轮播图的图片
    carouselImgUrls:[
      "https://vegetable666.oss-cn-shenzhen.aliyuncs.com/R-C.jpg",
      "https://vegetable666.oss-cn-shenzhen.aliyuncs.com/gif",
      "https://vegetable666.oss-cn-shenzhen.aliyuncs.com/sigua",
      "https://vegetable666.oss-cn-shenzhen.aliyuncs.com/fanqie",
      "https://vegetable666.oss-cn-shenzhen.aliyuncs.com/huanggua",
      "https://vegetable666.oss-cn-shenzhen.aliyuncs.com/ganlan"
    ],
    // 引入的全局变量
    globalData:'',
    // 搜索返回作物的值
    value:'',
    // 是否展示搜索列表
    showList:false,
    // 只能推荐返回的当地位置条件
    localWeather:'',
    // 智能推荐返回的作物
    referProducts:[],
    // 智能推荐的弹出层显示
    show:false,
    // 拍照无法识别弹出层
    showPP:false
  },
  onShow:function(){
    // 全局变量赋值
    this.setData({
      "globalData":app.globalData
    })
  },
  // 检查是否登录
  ifLogin(){
    login()
  },
  // 搜索框的值改变
  onChange(){
    console.log(this.data.value);
  },

  //搜索作物 
  onChange(e){
    if(e.detail==''){
      this.setData({
        'showList':false
      })
    }
    http(`/selectVe?name=${e.detail}`,'post').then(res=>{
      console.log("搜索返回的值");
      console.log(res);
      this.setData({
        'value':res.data,
        'showList':true
      })
    })
  },
  // 通过搜索到的进入详情页
  goDet(e){
    console.log("进入详情");
    console.log(e.target.dataset.val);
    app.globalData.detail=e.target.dataset.val
    wx.navigateTo({
      url: '../index_detail/index_detail',
    })
  },
  // 关闭搜索框
  closeSearch(){
    console.log("关闭搜索框");
    this.setData({
      'value':''
    })
  },


  // 拍照
  photo(){
    wx.chooseMedia({
      count: 1,
      sizeType: ['original'],
      success:(res)=>{

        wx.showToast({
          title: '正在上传中',
          icon:'loading',
          duration:500
        })
        
        console.log("获取图片返回的res");
        console.log(res);

        wx.uploadFile({
          filePath: res.tempFiles[0].tempFilePath,
          name: 'multipartFile',
          url: app.globalData.baseUrl+'/scan1',
          formData: {
            'multipartFile': res.tempFiles[0].tempFilePath
          },
          header:{
            Authorization :wx.getStorageSync('token')
          },
          complete:res=>{
            if(res.statusCode==200){
              console.log("图片上传返回值");
              console.log(res);
              // 传递过来的data是json字符串，有点问题，转换为对象
              const data = JSON.parse(res.data)
              console.log(data);
              // 跳转
              app.globalData.detail=data
              wx.navigateTo({
                url: '/pages/index_detail/index_detail',
              })
            }else{

              this.setData({
                'showPP':true
              })

            }
          }
        })
      },
      fail:(res)=>{
        wx.showToast({
          title: '上传失败',
          icon:'error',
          duration:1000
        })
      }
    })
  },

  // 推荐
  refer(){
    // 通过下面可以获取地理位置
    wx.getLocation({
      success:(res)=>{
        // 等待加载
        wx.showToast({
          title: '等待分析中',
          icon:'loading',
          duration:2000
        })
        console.log("获取砥砺位置返回的值");
        console.log(res);
        // 获取推荐作物
        http(`/LatLongitudeVegetables?lat=${res.latitude}&longitude=${res.longitude}`,'post').then(res=>{
          console.log("只能推荐借接口返回值");
          console.log(res);
          // 赋值给和弹出层显示
          this.setData({
            'referProducts':res.data,
            'show':true
          })
          console.log("本地的referProducts值");
          console.log(this.data.referProducts);
        })
        // 获取地理条件
        http(`/LatLongitudeWeather?lat=${res.latitude}&longitude=${res.longitude}`,'post').then(res=>{
          console.log("当前地理位置条件返回值");
          console.log(res);
          // 赋值
          this.setData({
            "localWeather":res.data.province.weather
          })
        })
      }
    })

  },
  // 关闭弹出层
  onClose(){
    this.setData({
      "show":false
    })
  },
  // 通过推荐进入详情
  detail(val){
    app.globalData.detail=val.target.dataset.val
    console.log("app和val");
    console.log(val);
    console.log(app);
    wx.navigateTo({
      url: '/pages/index_detail/index_detail',
    })
  },
  // 展示全部作物
  showPro(){
    wx.navigateTo({
      url: '/pages/index_allProduct/index_allProduct',
    })
  }
})
