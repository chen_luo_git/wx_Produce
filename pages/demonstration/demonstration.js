// pages/demonstration/demonstration.js
import http from '../../utils/httpRequst'
import login from '../../utils/login';
Page({

    /**
     * 页面的初始数据
     */
    data: {
      hm:true,
      resultShow:false,
      rankShow:false,
      wrongShow:false,
      types: ['随机','水果','蔬菜'],
      sure:false,
      test:false,
      loading:false,
      // 答题倒计时时间
      time:15*1000,
      timeData: {},
      // 环形进度条
      cir:100,
      // 问题
      questions:[],
      // 当前问题索引
      currentIndex:0,
      // 当前问题
      currentQuestion:{},
      // 问题数
      Qlimit:15,
      buttonColor:['linear-gradient(to right, #4bb0ff, #6149f6)','linear-gradient(to right, #4bb0ff, #6149f6)','linear-gradient(to right, #4bb0ff, #6149f6)','linear-gradient(to right, #4bb0ff, #6149f6)'],
      userAnswer:[],
      userWrongQ:[],
      isAnsing:true,
      rankData:[],
      rankLoading:true,
      score:0,
      scorePushLoading:false,
      addHonor:0,
      myHonor:0
    },

    // 检查是否登录
    ifLogin(){
      login()
    },
    // 显示排行榜
    toRank(){
      this.setData({
        hm:false
      })
      setTimeout(()=>{
        this.setData({
          rankShow:true
        })
        http('/answer/see1','post').then(res=>{
          console.log(res)
          this.setData({
            rankData:res.data.data,
            rankLoading:false
          })
        })
      },'300')
    },
    backToHm(){
      this.setData({
        rankShow:false
      })
      setTimeout(()=>{
        this.setData({
          hm:true,
          rankLoading:true,
          rankData:[]
        })
      },'300')
    },

    timeDown(e){
      this.setData({
        timeData: e.detail,
        cir:(e.detail.seconds/15)*100
      })
    },
    timeOut(){
      this.setData({
        isAnsing:false,
        userAnswer:[...this.data.userAnswer,{answerId:this.data.currentQuestion.id,answer:''}]
      })
      switch(this.data.currentQuestion.answer) {
        case 'A':
          this.setData({
            [`buttonColor[${0}]`]:'#07c160'
          })
          break;
        case 'B':
          this.setData({
            [`buttonColor[${1}]`]:'#07c160'
          })
          break;
        case 'C':
          this.setData({
            [`buttonColor[${2}]`]:'#07c160'
          })
          break;
        default:
          this.setData({
            [`buttonColor[${3}]`]:'#07c160'
          })
      } 
      setTimeout(()=>{
        this.setData({
          test:false
        })
        setTimeout(()=>{
          this.nextQ()
        },'500')
      },'1000')
    },

    timeStart() {
      const countDown = this.selectComponent('.control-count-down');
      countDown.start();
    },
  
    timePause() {
      const countDown = this.selectComponent('.control-count-down');
      countDown.pause();
    },
  
    timeReset() {
      const countDown = this.selectComponent('.control-count-down');
      countDown.reset();
    },

    startTest(){
      this.setData({
        hm:false,
      })
      setTimeout(()=>{
        this.setData({
          sure:true
        })
      },"300")
    },
    cancel(){
      this.setData({
        sure:false,
      })
      setTimeout(()=>{
        this.setData({
          hm:true
        })
      },"300")
    },
    yes(e){
      const { value } = e.detail;
      this.setData({
        sure:false,
        loading:true
      })
      if(value == '随机')
      {
        http('/answer/data','post').then (res=>{
          console.log(res)
          if(res.statusCode == 200)
          {
            this.setData({
              questions:res.data.data,
            })
            this.nextQ()
          }
        })
      }
      else
      {
        http(`/answer/type?type=${value}`,'post').then (res=>{
          console.log(res)
          if(res.statusCode == 200)
          {
            this.setData({
              questions:res.data.data,
            })
            this.nextQ()
          }
        })
      }
    },

    nextQ(){
      this.setData({
        buttonColor:['linear-gradient(to right, #4bb0ff, #6149f6)','linear-gradient(to right, #4bb0ff, #6149f6)','linear-gradient(to right, #4bb0ff, #6149f6)','linear-gradient(to right, #4bb0ff, #6149f6)'],
        cir:100
      })
      if(this.data.currentIndex != this.data.Qlimit)
      {
        this.setData({
          currentQuestion:this.data.questions[this.data.currentIndex]
        })
        this.setData({
          loading:false,
          currentIndex:this.data.currentIndex+1,
          test:true,
          isAnsing:true
        })
        this.timeReset()
        this.timeStart()
      }
      else
      {
        if(this.data.score == this.data.Qlimit)
        {
          this.setData({
            addHonor:1,
            resultShow:true
          })
        }
        else
        {
          this.setData({
            addHonor:0,
            resultShow:true
          })
        }
      }
    },
    rsClose(){
      this.setData({
        scorePushLoading:true
      })
      http('/answer/result','post',this.data.userAnswer).then(res=>{
        if(res.statusCode == 200)
        {
          wx.showToast({
            title: '成绩上传成功(^_^)',
          })
        }
        else
        {
          wx.showToast({
            icon:'error',
            title: '成绩上传失败(-_-)',
          })
        }
        this.setData({
          userAnswer:[],
          resultShow:false,
          score:0,
          addHonor:0,
          currentIndex:0,
          currentQuestion:{}
        })
        setTimeout(()=>{
          this.setData({
            hm:true,
          })
        },'300')
      })
    },

    userAns(e){
      if(!this.data.isAnsing)
      {
        return
      }
      this.setData({
        isAnsing:false,
        userAnswer:[...this.data.userAnswer,{answerId:this.data.currentQuestion.id,answer:e.currentTarget.dataset.ans}]
      })

      switch(this.data.currentQuestion.answer) {
        case 'A':
          this.setData({
            [`buttonColor[${0}]`]:'#07c160'
          })
          break;
        case 'B':
          this.setData({
            [`buttonColor[${1}]`]:'#07c160'
          })
          break;
        case 'C':
          this.setData({
            [`buttonColor[${2}]`]:'#07c160'
          })
          break;
        default:
          this.setData({
            [`buttonColor[${3}]`]:'#07c160'
          })
      } 

      this.timePause()
      if(this.data.currentQuestion.answer == e.currentTarget.dataset.ans)
      {
        this.setData({
          isAnsing:false,
          [`buttonColor[${e.currentTarget.dataset.ind}]`]:'#07c160',
          score:this.data.score+1
        })
      }
      else
      {
        this.setData({
          isAnsing:false,
          [`buttonColor[${e.currentTarget.dataset.ind}]`]:'#ee0a24'
        })
      }
      setTimeout(()=>{
        this.setData({
          test:false
        })
        setTimeout(()=>{
          this.nextQ()
        },'400')
      },'1000')
    },
    wrongClose(){
      this.setData({
        wrongShow:false,
        userWrongQ:[]
      })
      setTimeout(()=>{
        this.setData({
          hm:true
        })
      },'300')
    },
    toWrong(){
      this.setData({
        hm:false
      })
      http('/answer/wrong','post').then(res=>{
        console.log(res)
        this.setData({
          userWrongQ:res.data.data
        })
      })
      setTimeout(()=>{
        this.setData({
          wrongShow:true
        })
      },'300')
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})