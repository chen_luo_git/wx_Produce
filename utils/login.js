import http from '../utils/httpRequst';
export default()=>{
        console.log(wx.getStorageSync('token'));
        if(wx.getStorageSync('token')==''){
            // a是登录传递的参数，需要从wx.getUserProfile，和wx.login获得,同时可以传递头像和微信名称
            let a ={}

            wx.getUserProfile({
            desc:'获取用户资料',
            success:res=>{
                // 加载一个等待动画
                wx.showToast({
                title: '获取用户信息中',
                icon:'loading',
                duration:1100
                })
                // 登录需要上传的值
                a.encryptedData=res.encryptedData
                a.iv=res.iv
                a.userInfo=res.userInfo
                // 把获取到的头像渲染到页面
                wx.setStorageSync('avatarUrl', a.userInfo.avatarUrl)
                wx.setStorageSync('nickName', a.userInfo.nickName)
                // 通过wx.login获取登录需要的code
                wx.login({
                success:res=>{
                    a.code=res.code
                    // 发起登录请求
                        wx.request({
                            url: 'https://www.xieqinyu.com/login',
                            method:'post',
                            data:{
                            code:a.code,
                            encryptedData:a.encryptedData,
                            iv:a.iv,
                            userInfo:a.userInfo
                            },
                            header:{
                            Authorization:'xqy '
                            },
                            complete:res=>{
                            // 存储token到本地
                            console.log(res);
                            wx.setStorageSync('token', res.data.module.token)
                            wx.setStorageSync('openId', res.data.module.openId)
                            }
                        })
                },

                fail:res=>{
                    wx.showToast({
                    title: '获取用户code失败',
                    icon:'loading'
                    })
                }
                })
            },
            fail:res=>{
                wx.showToast({
                title: '获取用户失败',
                icon:'loading'
                })
            }
    })
        }else{
            console.log("存在token");
        }

}