// pages/myArticle/myArticle.js
import http from '../../utils/httpRequst'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        article:'',
        show:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        http('/discuss/MyArticle','post').then(res=>{
            if(res.data.data.length==0){
                console.log("还没有发布文章");
            }else{
                this.setData({
                    'article':res.data.data
                })
                // var arr = []
                // for(var i = 0 ; i<this.data.article.length ; i++){
                //     arr[i]=false
                // }
                // this.setData({
                //     'show':arr
                // })
                console.log(this.data.article);
            }
          })
    },

    // 显示详细信息
    showContent(e){
        // 判断现在是否为打开状态
        if(this.data.show[e.target.dataset.index]==true){
            this.setData({
                'show':[]
            })
            return
        }
        // 把当前的show改为打开状态
        let a =[]
        a[e.target.dataset.index]=true
        this.setData({
            'show':a
        })
    }
})